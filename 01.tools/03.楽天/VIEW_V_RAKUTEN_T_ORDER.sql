CREATE OR REPLACE VIEW TUHAN_USR1.V_RAKUTEN_T_ORDER
	(REC_NO, RAKUTEN_ORDERL_ID, SAKI_NM, SAKI_POSTCODE, TOINO, SYUKKABASYO_CNT, 
	SYUKKA_DATE_CNT, MSG) 
AS 
select tro.REC_NO,tro.RAKUTEN_ORDERL_ID,tro.SAKI_NM,tro.SAKI_POSTCODE,
       COALESCE(rd.TOINO_JSK,rd.TOINO_YOTEI) TOINO,                    -- 出荷実績の取り合わせ番号を先に採用
       rd.SYUKKABASYO_CNT,
       rd.SYUKKA_DATE_CNT,
       CASE
         WHEN rd.TOINO_JSK IS NULL AND rd.TOINO_YOTEI IS NULL THEN '送り状番号が入力されていません'
         WHEN rd.SYUKKABASYO_CNT > 1 THEN '複数の出荷拠点からの出荷です'
         WHEN rd.SYUKKA_DATE_CNT > 1 THEN '出荷日が分かれています'
       END MSG
  from T_RAKUTEN_ORDER tro                                             -- テンポラリテーブル
    left join V_RAKUTEN_ALL_DELI rd on rd.RAKUTEN_ORDER_ID = tro.RAKUTEN_ORDERL_ID and rd.SAKI_NM = tro.SAKI_NM and rd.SAKI_POSTCODE = tro.SAKI_POSTCODE
/
