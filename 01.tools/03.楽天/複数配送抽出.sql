CREATE OR REPLACE VIEW V_RAKUTEN_MULTIPLE_DELI
AS
 -- 有効な受注、配送の抽出
select oh.ORDER_ID,d.DELIVERY_ID,d.SYUKKA_DATE,GET_TOINO4DELIVERYID(d.DELIVERY_ID) TOINO,oh.CUSTOMER_NM,oh.CONTACT_ADDRESS,d.SAKI_NM,d.SAKI_ADDRESS
  from ORDER_HD oh
    inner join DELIVERY d on d.ORDER_ID = oh.ORDER_ID and d.DELETE_FLG = 0 and d.CANCEL_FLG = 0
 where oh.DELETE_FLG = 0
   and oh.CANCEL_FLG = 0
   and oh.ORDER_ROUTE = '10'                                                                                                 -- 楽天
   and oh.ORIGINAL_ID is not null                                                                                            -- CSV取り込み
   and oh.ORDER_ID not in
-- NOT(楽天受注で１配送で出荷実績がある)
(
select oh.ORDER_ID
  from ORDER_HD oh
 where oh.DELETE_FLG = 0
   and oh.CANCEL_FLG = 0
   and (select count(*) from DELIVERY d where d.ORDER_ID = oh.ORDER_ID and d.DELETE_FLG = 0 and d.CANCEL_FLG = 0) = 1   -- １配送
   and exists (select sj.DELIVERY_ID from SYUKKA_JSK sj where sj.ORDER_ID = oh.ORDER_ID and sj.OKURIJO_NBR is not null) -- 出荷済み
)