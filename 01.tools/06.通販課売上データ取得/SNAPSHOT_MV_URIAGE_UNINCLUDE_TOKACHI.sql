CREATE SNAPSHOT TUHAN_ANA.MV_URIAGE_UNINCLUDE_TOKACHI 
	TABLESPACE USERS LOGGING PCTFREE 10 MAXTRANS 255 
		STORAGE(INITIAL 64K NEXT 1M MINEXTENTS 1 MAXEXTENTS UNLIMITED ) 
	BUILD IMMEDIATE 
	REFRESH FORCE ON DEMAND START WITH TO_DATE('2020-10-22 03:10:00', 'YYYY-MM-DD HH24:MI:SS') NEXT SYSDATE + 1      
AS with tuhan_data as
(
select u.KEIJO_DAY,u.KEIJO_YM,u.ACCEPT_DATE,u.ACCEPT_YM,u.OEDER_ID,u.DELIVERY_ID,u.ROUTE_TYPE_DAI,u.ROUTE_TYPE_CHU,u.ROUTE_NM,u.PROD_CD,u.NAME,u.CLASS1NAME,u.CLASS2NAME,u.CLASS3NAME,u.QUANT
      ,u.KINGAKU,u.ZEINUKIKINGAKU
      ,(case u.CLASS1 when '09' then u.ZEINUKIKINGAKU else u.SYANAITOITUTANKA end) syanaitoitutanka --※会計コードであれば売上単価を単価とする
      ,(case u.CLASS1 when '09' then u.ZEINUKIKINGAKU * u.QUANT else u.GENKA end) genka
      ,u.DELI_CNT
  from
(
select to_number(fu.KEIJO_DAY) KEIJO_DAY,substr(fu.KEIJO_DAY,1,6) keijo_ym,oh.ACCEPT_DATE,trunc(oh.ACCEPT_DATE / 100) accept_ym,fu.OEDER_ID,fu.DELIVERY_ID,r.ROUTE_TYPE_DAI,r.ROUTE_TYPE_CHU,r.ROUTE_NM,fu.PROD_CD,i.NAME,i.CLASS1NAME,i.CLASS2NAME,i.CLASS3NAME,fu.QUANT
      ,fu.KINGAKU / ((mio.ZEIRT / 100) + 1 ) kingaku
      ,(case i.CLASS1 when '09' then fu.KINGAKU else i.PRICEA end) / ((mio.ZEIRT / 100) + 1 ) zeinukikingaku-- 単価（税抜）※会計コードであれば売上単価を単価とする
      ,mio.SYANAITOITUTANKA -- 社内統一原価単価
      ,fu.QUANT * mio.SYANAITOITUTANKA GENKA -- 原価金額
      ,d.DELI_CNT
      ,i.CLASS1
  from
(
select fu.KEIJO_DAY,fu.PROD_CD,fu.OEDER_ID,fu.DELIVERY_ID,sum(fu.QUANT) quant,sum(fu.KINGAKU) kingaku
  from
(
select fu.KEIJO_DAY,fu.PROD_CD,fu.QUANT,fu.TANKA,fu.QUANT * fu.TANKA kingaku,fu.OEDER_ID,fu.DELIVERY_ID
  from tuhan_usr1.FMMAXURIAGE fu
union all
select fu.KEIJO_DAY,fu.PROD_CD,fu.QUANT,fu.TANKA,fu.QUANT * fu.TANKA kingaku,fu.OEDER_ID,fu.DELIVERY_ID
  from tuhan_usr1.FMMAXDURIAGE fu
) fu
 group by fu.KEIJO_DAY,fu.PROD_CD,fu.OEDER_ID,fu.DELIVERY_ID
) fu
  inner join tuhan_usr1.V_ITEM i on i.ITEM_ID = fu.PROD_CD
  inner join tuhan_usr1.MV_ITEM_OTHER mio on mio.ITEM_ID = fu.PROD_CD
  inner join tuhan_usr1.ORDER_HD oh on oh.ORDER_ID = fu.OEDER_ID
    inner join tuhan_usr1.V_ORDER_ROUTE2 r on r.ROUTE = oh.ORDER_ROUTE
  inner join
(
select d.ORDER_ID,count(distinct d.SAKI_NM) deli_cnt
  from tuhan_usr1.DELIVERY d
 where d.DELETE_FLG = 0
 group by d.ORDER_ID
) d on d.ORDER_ID = fu.OEDER_ID -- 受注毎の配送数用

 where fu.KEIJO_DAY between (to_number(to_char(add_months(sysdate,-27),'YYYY')) * 10000 + 401) -- 全前年度
                        and to_number(to_char(sysdate - 1,'YYYYMMDD'))                         -- 前日
  -- 手数料、運賃を含めるため以下コメント
  --and i.CLASS1 not in ('09')
  -- とかちを除くため以下をコメントアウト
     and not (fu.PROD_CD between '00080000' and '00089999' or fu.PROD_CD = '00003894')  
) u
order by u.KEIJO_DAY,u.OEDER_ID,u.DELIVERY_ID,u.PROD_CD

),
fmmax_data
as
(
select *
  from
(
select to_number(ua.UDNDT) UDNDT,substr(ua.UDNDT,1,6) udndt_ym,'' ACCEPT_DATE,'' ACCEPT_YM,'' ORDER_ID,'' DELIVERY_ID,'茅野売店' ORDER_ROUTE_DAI,'茅野売店' ORDER_ROUTE_CHU,'茅野売店' ORDER_ROUTE,ua.HINCD,ha.HINNMA,ha.HINCLANM,ha.HINCLBNM,ha.HINCLCNM,ua.URISU,ua.URIKN / 1.08 URIKN,decode(ua.URISU,0,null,(ua.URIKN / 1.08) / ua.URISU) TANKA,ha.GENTITTK,ua.URISU * ha.GENTITTK genka,1 haisosu
 from 
(
select ua.UDNDT,ua.HINCD,sum(ua.URISU) URISU,min(ua.URITK) MIN_URITK,max(ua.URITK) MAX_URITK,sum(ua.URIKN) URIKN
  from
(
select ua.UDNDT,substr(ua.UDNDT,1,6) UDNDT_YM ,ua.HINCD,ua.URISU,ua.URITK,ua.URIKN
  from wat_usr1.UDNTRA@FMMAX_DB ua
 where ua.TOKCD = '999992'
   and ua.DATKB = '1'
    and not (ua.HINCD between '00080000' and '00089999' or ua.HINCD = '00003894')  -- とかち除く
   and ua.UDNDT between to_char(add_months(sysdate,-27),'YYYY') || '0401'
                    and to_number(to_char(sysdate -1,'YYYYMMDD'))
) ua
 group by ua.UDNDT,ua.HINCD
) ua
   inner join wat_usr1.HINMTA@FMMAX_DB ha on ha.HINCD = ua.HINCD
 --order by ua.UDNDT,ua.HINCD
union all
select to_number(ua.UDNDT) UDNDT,substr(ua.UDNDT,1,6),'','','','','とかち現金売上','とかち現金売上','とかち現金売上',ua.HINCD,ha.HINNMA,ha.HINCLANM,ha.HINCLBNM,ha.HINCLCNM,ua.URISU,ua.URIKN / 1.08,decode(ua.URISU,0,null,(ua.URIKN / 1.08) / ua.URISU),ha.GENTITTK,ua.URISU * ha.GENTITTK,1
 from 
(
select ua.UDNDT,ua.HINCD,sum(ua.URISU) URISU,min(ua.URITK) MIN_URITK,max(ua.URITK) MAX_URITK,sum(ua.URIKN) URIKN
  from
(
select ua.UDNDT,substr(ua.UDNDT,1,6) UDNDT_YM ,ua.HINCD,ua.URISU,ua.URITK,ua.URIKN
  from wat_usr1.UDNTRA@FMMAX_DB ua
 where ua.TOKCD = '999995'
   and ua.DATKB = '1'
    and not (ua.HINCD between '00080000' and '00089999' or ua.HINCD = '00003894')  -- とかち除く
   and ua.UDNDT between to_char(add_months(sysdate,-27),'YYYY') || '0401'
                    and to_number(to_char(sysdate -1,'YYYYMMDD'))
) ua
 group by ua.UDNDT,ua.HINCD
) ua
   inner join wat_usr1.HINMTA@FMMAX_DB ha on ha.HINCD = ua.HINCD
) ua
 order by ua.ORDER_ROUTE desc,ua.UDNDT,ua.HINCD


)

select t.KEIJO_DAY,t.KEIJO_YM,t.ACCEPT_DATE,t.ACCEPT_YM,t.OEDER_ID,t.DELIVERY_ID,t.ROUTE_TYPE_DAI,t.ROUTE_TYPE_CHU,t.ROUTE_NM
      ,t.PROD_CD,t.NAME,t.CLASS1NAME,t.CLASS2NAME,t.CLASS3NAME
      ,t.QUANT,t.KINGAKU,t.ZEINUKIKINGAKU,t.SYANAITOITUTANKA,t.GENKA
      ,t.DELI_CNT
  from tuhan_data t

union all

select f.UDNDT,f.UDNDT_YM,CAST(f.ACCEPT_DATE AS NUMBER),to_number(f.ACCEPT_YM),f.ORDER_ID,f.DELIVERY_ID,f.ORDER_ROUTE_DAI,f.ORDER_ROUTE_CHU,f.ORDER_ROUTE
      ,f.HINCD,trim(f.HINNMA),trim(f.HINCLANM),trim(f.HINCLBNM),trim(f.HINCLCNM)
      ,f.URISU,f.URIKN,to_number(f.TANKA),f.GENTITTK,f.GENKA
      ,f.HAISOSU
  from fmmax_data f
/
